# serverless_demo



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/tan.csell/serverless_demo.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/tan.csell/serverless_demo/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
## Giới thiệu về Serverless Framework:
- Serverless là môi trường, nền tảng để thực thi các ứng dụng và dịch vụ mà không cần quan tâm đến máy chủ. Đối với Serverless, bạn sẽ không phải quan tâm đến việc phân bổ, quản lý tài nguyên hệ điều hành hay những vấn đề về nâng cấp, bảo mật. Với môi trường Serverless, bạn chỉ cần tập trung phát triển sản phẩm còn vấn đề vận hành như thế nào sẽ do nền tảng này đảm nhiệm.

- Điều quan trọng và khác biệt trong môi trường Serverless là bạn là bạn phải trả phí cho những phần bạn sử dụng. Giả sử đối với một máy chủ ảo, phí trọn gói với thời gian chạy 24/7 trong vòng 1 tháng, RAM và CPU, băng thông, lưu trữ. Bạn vẫn sẽ phải trả phí đều đặn mỗi tháng mặc dù máy chủ ảo đó không chạy hoặc bạn chỉ sử dụng một phần nhỏ công suất thì bạn vẫn phải chi trả cho cả gói. Bạn có thể hiểu một cách đơn giản, Serverless giống như một gói mạng hàng tháng bạn đăng ký, dù bạn dùng hay không thì vẫn phải chịu phí này hàng tháng.
Môi trường Serverless được cấu tạo từ 5 thành phần chính sau:

- Authentication Service (máy chủ xác thực): là loại máy chủ mạng mà người dùng xác thực từ xa hoặc CNTT node kết nối với một ứng dụng hoặc dịch vụ.

- Product Database (cơ sở dữ liệu sản phẩm): tất cả các dữ liệu sẽ được chuyển đến  kho quản lý, các kho này sẽ được chia nhỏ ra cho những khách hàng lẻ để tránh sự  quá tải.

- Client (máy khách): với hai sự thay đổi trên, một vài logic sẽ được nằm ở phía client như user session (bạn sẽ thấy rõ nhất tại các Single Page App), phần giao diện hiển thị, route nào user có thể truy cập trong code client.

- Search Function (chức năng tìm kiếm): một vài ràng buộc vẫn được server nắm như chức năng tìm kiếm. Các bạn có thể gọi các API Gateway, những yêu cầu từ client, HTTP sẽ lấy dữ liệu từ kho và trả về cho chúng ta.

- Purchase Function (chức năng đặt hàng): đây là tính năng do một nhà cung cấp khác. Những logic khác nhau sẽ được tách ra và deploy thành những khối khác nhau. Đây cũng là cách tiếp cận rất phổ biến trong Microservices.
## Ưu và nhược điểm của Serverless
* Ưu điểm của Serverless là gì?

- Để xây dựng nên một ứng dụng Serverless đồng nghĩa với việc bạn chỉ tập trung vào sản phẩm cốt lõi mà không cần phải quan tâm đến vấn đề quản lý và vận hành của hệ thống máy chủ. Từ đó, các nhà phát triển có thể dành nhiều thời gian và năng lượng hơn cho việc xây dựng các sản phẩm tuyệt vời có tính ổn định và linh hoạt cao.

- Nói về những lợi ích của Serverless, ta có thể kể đến như:

- Không cần quản lý máy chủ: Bạn sẽ không cần mất thời gian và công sức để duy trì bất kỳ máy chủ nào. Sẽ không còn bất kỳ vấn đề nào về cài đặt, nâng cấp hoặc quản trị máy chủ.

- Thay đổi quy mô một cách linh hoạt: Việc thay đổi sang quy mô tự động bằng cách điều chỉnh dung lượng thông qua chỉnh đổi đơn vị sử dụng đơn giản hơn rất nhiều so với máy chủ độc lập.

- Độ sẵn sàng cao: Ứng dụng Serverless được đánh giá cao về độ sẵn sàng tích hợp và tính đúng sai. Bạn sẽ không phải tạo kiến trúc cho các khả năng này bởi các dịch vụ ứng dụng đã cung cấp sẵn theo kiểu mặc định. Ngoài ra, bạn cũng có thể chọn trung tâm dữ liệu ở một hoặc nhiều nơi để triển khai sản phẩm của mình một cách dễ dàng.

- Tiết kiệm chi phí: Sử dụng Serverless đã giúp tiết kiệm rất nhiều chi phí cần bỏ ra để cấu hình, cài đặt và bảo trì máy chủ.

* Nhược điểm của Serverless

- Serverless được đánh giá là một ý tưởng tuyệt vời nhưng không hoàn hảo. Serverless có những nhược điểm riêng, do vậy các bạn cũng cần lưu ý trước khi sử dụng.

- Độ trễ: Hiệu suất của Serverless có thể là một vấn đề. Bản thân mô hình này đã gây nên độ trễ lớn trong quá trình phản hồi lại với các lệnh của ứng dụng. Nếu khách hàng yêu cầu hiệu suất cao, bạn nên sử dụng các máy chủ ảo phân bổ sẽ ưu việt hơn.

- Gỡ lỗi: Việc giám sát và gỡ lỗi của Serverless cũng gặp nhiều khó khăn. Việc không sử dụng tài nguyên máy chủ thống nhất dẫn đến việc hoạt động gặp nhiều trở ngại.

- Giới hạn bộ nhớ, thời gian: Các nhà cung cấp thường giới hạn tài nguyên ở mức cố định về bộ nhớ và thời gian thực thi. Giả sử thời gian thực thi tối đa là 5 phút, sau 5 phút, quá trình thực thi này sẽ bị ngắt. Về bộ nhớ thì sẽ được giới hạn nhiều mức khác nhau tùy theo nhà cung cấp.

- Phụ thuộc nhà cung cấp: Bạn sẽ không thể tùy ý chạy các phiên bản của phần mềm chính xác như mình mong muốn mà phải phụ thuộc vào bên cung cấp. Ví dụ bạn cần Nodejs 10.x nhưng nhà cung cấp lại chỉ hỗ trợ đến bản 8.x thì bạn sẽ không thể dùng phiên bản này.

- Chi phí ngầm: Chi phí này sẽ phụ thuộc vào nhà cung cấp có tính hay không, tuy nhiên những chi phí có thể phát sinh như: lưu trữ mã nguồn, lưu trữ dữ liệu, băng thông. Nếu không tối ưu đúng cách, có thể chi phí ngầm sẽ cao hơn chi phí của Serverless.

## Những nội dung thực hành với serverless bao gồm: 
## 1.Setting Up the Serverless Framework with AWS - Configure AWS Credentials
- create account with aws 
- setup and install aws in the browser
## 2.Creating a new Serverless Project and deploying a Lambda
- create a project in the browser
- deploy to Lambda in AWS
- Run funtion in project
## 3.Deploy an S3 bucket and Upload Data
- How to provision an S3 bucket
- How to install a Serverless plugin
- How to automatically upload content to an S3 bucket using serverless-s3-sync
## 4.Creating an API with Serverless - API Gateway and AWS Lambda
- How to set up a Lambda in Serverless
- What response you need to return to work with API Gateway
- How to deploy the Lambda and API Gateway using Serverless
- How to test GET requests in the browser
## 5.Adding Serverless Webpack to your Project
- With Serverless Webpack it compiles all the code that the Lambda needs into a single file, excluding any unused code. This makes it much smaller, reducing the unzipped errors.
## 6.Create a Serverless Database - DynamoDB with the Serverless Framework
- How to create a serverless database with DynamoDB. 
- Being able to create these autoscaling tables will enable you to create much more powerful applications.
- Note: Whether it's storing user scores, product information or content, Dynamo is a great tool to be able to use.
## 7.Create an API to get data from DynamoDB Database
- Creating multiple endpoints on an API
- Give permission to a lambda to access DynamoDB
- Create a reusable Dynamo wrapper to make accessing data easier
## 8.Adding New Data to DynamoDB Database
- Add data to a dynamoDB table
- Set up a api create method POST in Serverless
- Test api POST using Postman
## 9.Adding and Getting Files from Amazon S3 with Serverless.
- Added two more api to serverless application
- Created a wrapper to easily read and write files from Amazon S3
- Uploaded JSON data to S3 bucket
- Downloaded JSON data from S3 bucket
## 10.Securing Serverless API With API Keys (Ver )
- How to create API keys so you can control who can access your APIs
## 11.How to send an email with Amazon SES and Serverless
- Set up an API to send an email with Amazon Simple Email Service (SES)