const AWS = require('aws-sdk');
const Responses = require('../common/Api_Responses');
const SES = new AWS.SES();

exports.handler = async (event) => {
	console.log('event', event);
	const { to, from, subject, text } = JSON.parse(event.body)

	if (!to || !from || !subject || !text) {
		// return err
		return Responses._400({ message: 'to, from, subject, text is required' });
	}

	const params = {
		Destination: {
			ToAddresses: [to],
		},
		Message: {
			Body: {
				Text: { Data: text },
			},
			Subject: { Data: subject },
		},
		Source: from,
	}

	try {
		await SES.sendEmail(params).promise()
		return Responses._200({ message: "Success" })
	} catch (error) {
		console.log('Error sending email', error)
		return Responses._400({ message: "The email failed to send`" })
	}
};


