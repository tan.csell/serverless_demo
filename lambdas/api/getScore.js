const Responses = require('../common/Api_Responses');
const Dynamo = require('../common/dynamo');
const tableName = process.env.TableName;

exports.handler = async (event) => {
	console.log('event', event);
	if (!event.pathParameters || !event.pathParameters.ID) {
		// check ID is valid in param
		return Responses._400({ message: 'Missing ID from path' });
	}

	// check id in data is valid
	const ID = event.pathParameters.ID;

	const aUser = await Dynamo.get(ID, tableName).catch((err) => {
		console.log('error in Dynamo Get', err);
		return null;
	});

	if (aUser) {
		return Responses._200(aUser);
	}

	// return err if ID not in data
	return Responses._400({ message: 'User not found' });
};
