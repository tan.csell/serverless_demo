const Responses = require('../common/Api_Responses');
const Dynamo = require('../common/dynamo');
const tableName = process.env.TableName;

exports.handler = async (event) => {
	console.log('event', event);
	if (!event.pathParameters || !event.pathParameters.ID) {
		// check ID is valid in param
		return Responses._400({ message: 'Missing ID from path' });
	}

	const aUser = JSON.parse(event.body);
	aUser.ID = event.pathParameters.ID;

	const newUser = await Dynamo.write(aUser, tableName).catch(err => {
		console.log(err)
		return err;
	})

	if (newUser) {
		return Responses._200(newUser);
	}

	// return err if ID not in data
	return Responses._400({ message: 'User not create' });
};
