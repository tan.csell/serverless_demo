const Responses = require('../common/Api_Responses');
const S3 = require('../common/S3');
const bucket = process.env.BucketName;

exports.handler = async (event) => {
	console.log('event', event);
	if (!event.pathParameters || !event.pathParameters.fileName) {
		// check ID is valid in param
		return Responses._400({ message: 'Missing file from path' });
	}

	const fileName = event.pathParameters.fileName;
	const data = JSON.parse(event.body);

	const newData = await S3.write(data, fileName, bucket).catch(err => {
		console.log(err)
		return null;
	})

	if (newData) {
		return Responses._200(newData);
	}

	// return err if ID not in data
	return Responses._400({ message: 'Upload file failed!' });
};
