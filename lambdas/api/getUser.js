const Responses = require('../common/Api_Responses');

exports.handler = async (event) => {
    console.log('event', event);
    if (!event.pathParameters || !event.pathParameters.ID) {
        // check ID is valid in param
        return Responses._400({ message: 'Missing ID from path' });
    }

    // check id in data is valid
    const ID = event.pathParameters.ID;
    if (data[ID]) {
        return Responses._200(data[ID]);
    }

    // return err if ID not in data
    return Responses._400({ message: 'ID not in data' });
};

const data = {
    1: { name: 'Nam blu', age: 20, address: 'Bac Ninh' },
    2: { name: 'Hoc blu', age: 20, address: 'Hai Phong' },
    3: { name: 'Minh blu', age: 20, address: 'Ha Noi' },
};
