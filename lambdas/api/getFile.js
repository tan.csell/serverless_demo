const Responses = require('../common/Api_Responses');
const S3 = require('../common/S3');
const bucket = process.env.BucketName;

exports.handler = async (event) => {
	console.log('event', event);
	if (!event.pathParameters || !event.pathParameters.fileName) {
		// check ID is valid in param
		return Responses._400({ message: 'Missing file from path' });
	}

	const fileName = event.pathParameters.fileName;

	const file = await S3.get(fileName, bucket).catch((err) => {
		console.log("Can't get file from aws:", err);
		return null;
	});

	if (file) {
		return Responses._200(file);
	}

	// return err if ID not in data
	return Responses._400({ message: 'Get file failed!' });
};
