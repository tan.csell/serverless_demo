const AWS = require('aws-sdk');
const documentClient = new AWS.DynamoDB.DocumentClient();
const Dynamo = {
	async get(ID, TableName) {
		const params = {
			TableName,
			Key: {
				ID,
			},
		};
		const data = await documentClient.get(params).promise();
		if (!data || !data.Item) {
			throw Error(`${ID} from ${TableName} is not valid`);
		}
		return data.Item;
	},
	async write(data, TableName) {
		if (!data.ID) {
			throw Error('No ID');
		}

		const params = {
			TableName,
			Item: data,
		};
		const res = await documentClient.put(params).promise();
		if (!res) {
			throw Error(`There was an error: ${data.ID} from ${TableName}`)
		}
		return data;
	},
};
module.exports = Dynamo;
