const AWS = require('aws-sdk');
const Responses = require('./Api_Responses');
const s3Client = new AWS.S3();

const S3 = {
    async get(fileName, bucket) {
        const params = {
            Bucket: bucket,
            Key: fileName,
        };
        let data = await s3Client.getObject(params).promise();
        if (
            data &&
            data.Body &&
            fileName.slice(fileName.length - 4, fileName.length) == 'json'
        ) {
            data = data.Body.toString();
            return data;
        }

        throw Error({
            message: `error get file ${fileName} from ${bucket}`,
        });
    },
    async write(data, fileName, bucket) {
        const params = {
            Bucket: bucket,
            Body: JSON.stringify(data),
            Key: fileName,
        };
        const newData = await s3Client.putObject(params).promise();
        if (newData) {
            return newData;
        }

        throw Error({
            message: 'error when writing file from param',
        });
    },
};
module.exports = S3;
